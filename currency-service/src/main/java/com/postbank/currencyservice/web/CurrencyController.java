package com.postbank.currencyservice.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.postbank.currencyservice.model.dto.CurrencyDto;
import com.postbank.currencyservice.model.dto.IncomingMessage;
import com.postbank.currencyservice.model.dto.OutgoingMessage;
import com.postbank.currencyservice.service.CurrencyExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Controller
public class CurrencyController {

    @Autowired
    private CurrencyExchangeService service;



    @GetMapping("/download-currencies")
    public ResponseEntity<List<CurrencyDto>> getCurrentCurrencyStatus() throws JsonProcessingException {
       return ResponseEntity.ok(service.dailyCurrencyRateRequest());
    }


//    @MessageMapping("/process-message")
//    @SendTo("/currencies/public")// app/message will be the endpoint we should send the request
//    public OutgoingMessage receivePublicMessage(@RequestBody IncomingMessage message) {
//        OutgoingMessage message1 = new OutgoingMessage();
//        message1.setContent(message.getName());
//        return message1;
//    }

}
