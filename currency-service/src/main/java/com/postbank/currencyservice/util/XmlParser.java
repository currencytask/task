package com.postbank.currencyservice.util;

import org.springframework.stereotype.Component;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;

@Component
public class XmlParser {


    @SuppressWarnings("unchecked")
    public <T> T toJavaObject(String xmlAsString, Class<T> object) {
        try {
            StringReader sr = new StringReader(xmlAsString);
            JAXBContext jaxbContext = JAXBContext.newInstance(object);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (T) unmarshaller.unmarshal(sr);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }

    }

}