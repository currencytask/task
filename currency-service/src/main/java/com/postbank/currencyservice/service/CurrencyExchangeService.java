package com.postbank.currencyservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.postbank.currencyservice.model.dto.CurrencyDto;
import com.postbank.currencyservice.model.dto.IncomingMessage;
import com.postbank.currencyservice.model.dto.RowsetDto;
import com.postbank.currencyservice.model.entity.Currency;
import com.postbank.currencyservice.repository.CurrencyRepository;
import com.postbank.currencyservice.util.JsonParser;
import com.postbank.currencyservice.util.XmlParser;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CurrencyExchangeService {

    final String uriToBnb = "https://www.bnb.bg/Statistics/StExternalSector/StExchangeRates/StERForeignCurrencies/index.htm?download=xml&search=&lang=BG";

    @Autowired
    private XmlParser xmlParser;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CurrencyRepository repository;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private JsonParser jsonParser;


    public List<CurrencyDto> dailyCurrencyRateRequest() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> forEntity = restTemplate.getForEntity(uriToBnb, String.class);
        String xmlAsString = forEntity.getBody().toString();
        int indexOfStartingRow = xmlAsString.indexOf("ROWSET");

        String currenciesAsString = xmlAsString.substring(indexOfStartingRow - 1);

        RowsetDto allCurrencies = xmlParser.toJavaObject(currenciesAsString, RowsetDto.class);

        List<CurrencyDto> currenciesWithoutFirstEmptyRow =
                allCurrencies.getRow().stream().filter(dto -> dto.getRatio() != null).collect(Collectors.toList());

        List<Currency> currencyCollection = currenciesWithoutFirstEmptyRow.stream().map(dto -> {
            Currency map = modelMapper.map(dto, Currency.class);
            int year = Integer.parseInt(dto.getCurrDate().substring(dto.getCurrDate().length() - 4));
            int month = Integer.parseInt(dto.getCurrDate().substring(3, 5));
            int day = Integer.parseInt(dto.getCurrDate().substring(0, 2));
            map.setCurrDate(LocalDate.of(year,Month.of(month),day));
            return map;
        }).toList();

        repository.saveAll(currencyCollection);

        IncomingMessage incomingMessage = new IncomingMessage()
                    .setContent(jsonParser.objectToString(currenciesWithoutFirstEmptyRow));

            messagingTemplate.convertAndSend("/currencies/public", incomingMessage);

        return currenciesWithoutFirstEmptyRow;
    }


}
