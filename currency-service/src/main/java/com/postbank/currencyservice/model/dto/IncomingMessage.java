package com.postbank.currencyservice.model.dto;

public class IncomingMessage {

    private String content;

    public String getContent() {
        return content;
    }

    public IncomingMessage setContent(String content) {
        this.content = content;
        return this;
    }

    public IncomingMessage(String content) {
        this.content = content;
    }

    public IncomingMessage() {
    }

}