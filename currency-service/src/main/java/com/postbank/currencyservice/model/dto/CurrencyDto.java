package com.postbank.currencyservice.model.dto;


import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;


@XmlRootElement(name = "ROW")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class CurrencyDto {

    @XmlElement(name = "NAME_")
    private String name;
    @XmlElement(name = "CODE")
    private String code;
    @XmlElement(name = "RATIO")
    private Integer ratio;

    @XmlElement(name = "REVERSERATE")
    private BigDecimal reverserate;
    @XmlElement(name = "RATE")
    private BigDecimal rate;

    @XmlElement(name = "CURR_DATE")
    private String currDate;

}
