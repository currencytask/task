package com.postbank.currencyservice.model.dto;

import com.postbank.currencyservice.model.entity.Currency;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "ROWSET")
@XmlAccessorType(XmlAccessType.FIELD)
public class RowsetDto {

    @XmlElement(name = "ROW")
    private List<CurrencyDto> row;


}
