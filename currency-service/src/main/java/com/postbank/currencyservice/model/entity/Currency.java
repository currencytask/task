package com.postbank.currencyservice.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;


@Table(name = "currency")
@Entity
@Getter
@Setter
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;
    @Column
    private String code;
    @Column
    private Integer ratio;

    @Column(columnDefinition = "DECIMAL")
    private BigDecimal reverserate;
    @Column(columnDefinition = "DECIMAL")
    private BigDecimal rate;

    @Column(columnDefinition = "DATE")
    private LocalDate currDate;


}
