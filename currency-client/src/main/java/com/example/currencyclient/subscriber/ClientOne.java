package com.example.currencyclient.subscriber;

import com.example.currencyclient.connectionhandler.ClientOneSessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;


@Component
public class ClientOne implements CommandLineRunner {

    @Autowired
    private ClientOneSessionHandler clientOneSessionHandler;

    @Override
    public void run(String... args) throws Exception {
        WebSocketClient client = new StandardWebSocketClient();

        WebSocketStompClient stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        ListenableFuture<StompSession> sessionAsync =
                stompClient.connect("ws://localhost:8080/currency-server", clientOneSessionHandler);
        StompSession session = sessionAsync.get();
        session.subscribe("/currencies/public", clientOneSessionHandler);
    }
}


