package com.example.currencyclient.service;

import com.example.currencyclient.model.entity.CurrencyDto;
import com.example.currencyclient.model.entity.CurrencyDtoList;
import com.example.currencyclient.model.entity.OutgoingMessage;
import com.example.currencyclient.util.JsonParser;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Currency;
import java.util.List;


@Service
public class JsonToCurrencyMapperService {

    @Autowired
    private JsonParser parser;

    @Autowired
    private CurrencyService service;

    public void jsonStringToCurrency(OutgoingMessage message) {
        String jsonAsString = message.getContent();
        List<CurrencyDto> currencyDtos = parser.jsonToCurrencyDto(jsonAsString);
        service.saveAllCurrencies(currencyDtos);
    }

}
