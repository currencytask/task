package com.example.currencyclient.service;

import com.example.currencyclient.model.entity.Currency;
import com.example.currencyclient.model.entity.CurrencyDto;
import com.example.currencyclient.repository.CurrencyRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CurrencyService {

    @Autowired
    private CurrencyRepository repository;

    @Autowired
    private ModelMapper mapper;


    public void saveAllCurrencies(List<CurrencyDto> currencyDtos){
        LocalDate now = LocalDate.now();
        Optional<Currency> firstByDateOfEnrollingCurrency = repository.findFirstByDateOfEnrollingCurrency(now);
        if(firstByDateOfEnrollingCurrency.isEmpty()) {
            List<Currency> collection = currencyDtos.stream().map(dto -> {
                Currency map = mapper.map(dto, Currency.class);
                int year = Integer.parseInt(dto.getCurrDate().substring(dto.getCurrDate().length() - 4));
                int month = Integer.parseInt(dto.getCurrDate().substring(3, 5));
                int day = Integer.parseInt(dto.getCurrDate().substring(0, 2));
                map.setCurrDate(LocalDate.of(year, Month.of(month),day));
                map.setDateOfEnrollingCurrency(LocalDate.now());
                return map;
            }).collect(Collectors.toList());

            repository.saveAll(collection);
        }
    }

}
