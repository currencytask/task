package com.example.currencyclient.connectionhandler;

import com.example.currencyclient.model.entity.OutgoingMessage;
import com.example.currencyclient.service.JsonToCurrencyMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

@Component
public class ClientOneSessionHandler extends StompSessionHandlerAdapter {

    private JsonToCurrencyMapperService service;

    public ClientOneSessionHandler(JsonToCurrencyMapperService service) {
        this.service = service;
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return OutgoingMessage.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
            OutgoingMessage outgoingMessage = new OutgoingMessage();
            outgoingMessage.setContent(((OutgoingMessage) (payload)).getContent());
            service.jsonStringToCurrency(outgoingMessage);
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        System.out.println("Connected to WebSocket");
    }

}