package com.example.currencyclient.repository;

import com.example.currencyclient.model.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency,Long> {

    Optional<Currency> findFirstByDateOfEnrollingCurrency(LocalDate dateOfEnrollingCurrency);


}
