package com.example.currencyclient.model.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class CurrencyDto {

    @SerializedName("name")
    private String name;
    @SerializedName("code")
    private String code;
    @SerializedName("ratio")
    private Integer ratio;
    @SerializedName("reverserate")
    private BigDecimal reverserate;
    @SerializedName("rate")
    private BigDecimal rate;
    @SerializedName("currDate")
    private String currDate;


}
