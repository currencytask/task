package com.example.currencyclient.model.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "currency")
public class Currency {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "code")
    private String code;
    @Column(name = "ratio")
    private Integer ratio;
    @Column(columnDefinition = "DECIMAL")
    private BigDecimal reverserate;
    @Column(columnDefinition = "DECIMAL", name = "rate")
    private BigDecimal rate;
    @Column(columnDefinition = "DATE")
    private LocalDate currDate;
    @Column(name = "date_of_enroll",columnDefinition = "DATE")
    private LocalDate dateOfEnrollingCurrency;

}
