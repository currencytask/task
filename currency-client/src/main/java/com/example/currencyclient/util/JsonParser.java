package com.example.currencyclient.util;

import com.example.currencyclient.model.entity.CurrencyDto;
import com.example.currencyclient.model.entity.CurrencyDtoList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;

@Component
public class JsonParser {

    @Autowired
    private Gson gson;

    public List<CurrencyDto> jsonToCurrencyDto(String json) {
        Type listType = new com.google.gson.reflect.TypeToken<List<CurrencyDto>>() {}.getType();
        return gson.fromJson(json, listType);
    }
}
